//Alternative solution in Java instead of the expected solution in PHP.

public class Exercise3 {
	
    public static void main(String[] args) 
    { 
//        String str1 = "MICHAEL"; 
//        String str2 = "JORDAN"; 
    	String str1 = "DEEPIKA";
    //	String str2 = "RAO";
    	String str2 =  "";
        System.out.println("Final Merged String : "+ mergeTwoStrings(str1, str2)); 
    } 
	
    public static String mergeTwoStrings(String str1, String str2) 
    {  
        StringBuilder finalString = new StringBuilder(); 
        
        if(str1 != null && str2 != null)
        {	
	        for (int i = 0; i < str1.length() || i < str2.length(); i++) { 
	  
	            if (i < str1.length()) 
	            	finalString.append(str1.charAt(i)); 
	  
	            if (i < str2.length()) 
	            	finalString.append(str2.charAt(i)); 
	        } 
        }
  
        return finalString.toString(); 
    } 
}
