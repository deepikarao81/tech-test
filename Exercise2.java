
public class Exercise2 {
	
	public static void main(String[] args) {
		//int adjDigits = 3;
		int adjDigits = 5;
		long product = largestProduct("1027839594", adjDigits);
		if(product == 0)
			System.out.println("Insufficient length of the number!");
		else
			System.out.println("Value : "+ product);
	}

	public static long largestProduct(String inputNumStr, int adjDigits) {

		long finalSum = 0;
		long temp = 0;
		if(inputNumStr == null || "".equals(inputNumStr))
			return 0L;
		
		char[] inputNums = inputNumStr.toCharArray();

		for (int i = 0; i < inputNums.length - adjDigits; i++) {
			
			temp = Integer.parseInt(String.valueOf(inputNums[i]));
			for (int j = 1; j < adjDigits; j++) { //iterate the next adjDigits digits
				temp = temp * Integer.parseInt(String.valueOf(inputNums[i+j]));
			}
			if (finalSum < temp) {
				finalSum = temp;
			}
		}
		
		return finalSum;
	}

}
