import java.util.HashSet;

public class Exercise1 {
	public static void main(String[] args) {
		String[] inputStrs = new String[] {"documentarily", "aftershock", "countryside", "six-year-old", "Double-down", "Euclidean", "epidemic"};
		for (int i = 0; i < inputStrs.length; i++) {
			System.out.println("Validity of the input string: "+ inputStrs[i] + " is: "+ checkForDuplicate(inputStrs[i]));
		}
	}
	
	public static boolean checkForDuplicate(String inputStr) {
		HashSet<Character> uniqueChars = new HashSet<Character>();
		boolean hasDuplicate = true;
		
		if(inputStr != null) {
			for(int i = 0; i<inputStr.length(); i++) {
				char c = inputStr.charAt(i);
				if(uniqueChars.contains(c) && Character.isAlphabetic(c)) { //needs check for alphabet and skip others
					hasDuplicate = false;
					System.out.println("Found a duplicate!");
					break;
				}
				else {
					uniqueChars.add(Character.toLowerCase(c));
				}
			}
		}
		return hasDuplicate;
		
	}

}
