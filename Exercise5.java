import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerFactoryConfigurationError;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class Exercise5 {
	
	public static void main(String[] args) {
		Exercise5 ex5 = new Exercise5();
		HashMap<String, String> propertyTypeMap = ex5.buildPropertyMap();
		if(propertyTypeMap == null)
			System.out.println("System error!");
		else
			System.out.println("Final property map : " + propertyTypeMap);
	}

	public HashMap<String, String> buildPropertyMap() {

		HashMap<String, String> propertyTypeMap = new HashMap<String, String>();
		
		ClassLoader classLoader = this.getClass().getClassLoader();
		URL fileurl = classLoader.getResource("sample-reaxml.xml");
		if(fileurl == null)
			return null;
		
		File file = new File(fileurl.getFile());
		
		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(file);

			NodeList propNodes = doc.getElementsByTagName("propertyList");
			
			Node root = propNodes.item(0);
			NodeList propChildNodes = root.getChildNodes();

			for (int i = 0; i < propChildNodes.getLength(); i++) {
				Node n = propChildNodes.item(i);

				if (n.getNodeType() == Node.ELEMENT_NODE) {
					Element propType = (Element) n;

					NodeList mapEle = propType.getElementsByTagName("uniqueID");
					if (mapEle != null && mapEle.getLength() > 0) {
						Element mapObj = (Element) mapEle.item(0);

						propertyTypeMap.put(mapObj.getTextContent(), propType.getNodeName());
					}
				}

			}
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (TransformerFactoryConfigurationError e) {
			e.printStackTrace();
		} catch (org.xml.sax.SAXException e) {
			e.printStackTrace();
		}

		return propertyTypeMap;
	}

}
