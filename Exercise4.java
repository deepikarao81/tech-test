public class Exercise4 {
	
	public static void main(String[] args) {
		
		int inputNum = 3452;
		
		int length = String.valueOf(inputNum).length();
		while(length != 1) { //call until we get single digit number
			inputNum = getSingleDigitNumber(inputNum);
			length = String.valueOf(inputNum).length();
		}
		System.out.println("Final Sum : "+inputNum);
	}
	
	public static int getSingleDigitNumber(int inputNum) {
		int sum = 0;
		int temp = 0;
		
		while(inputNum > 0)
        {
            temp = inputNum % 10;
            sum = sum + temp;
            inputNum = inputNum / 10;
        }
        return sum;
	}

}
